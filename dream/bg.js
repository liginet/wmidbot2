var WMIDBs;
var re_photos = [];
var re_tps = [];
var re_fav = [];
var re_blist = [];
var re_blist_chat = [];
var re_log = [];
function get_photos(){
	WMIDBs.transaction(function(transaction) {
			transaction.executeSql("SELECT * FROM photos", [], function(transaction, results){ 
				 for (var i=0; i<results.rows.length; i++) {
					var row = results.rows.item(i);	
					re_photos.push({id:row['id'],i:row['base']});
				 }
			}, DB.errorHandler);
		}
	);
}
function get_tpls(){
	var n = 0;
	var x = 0;
	WMIDBs.transaction(function(transaction) {
			transaction.executeSql("SELECT * FROM tpl ORDER BY id desc", [], function(transactions, results){ 
				 for (var i=0; i<results.rows.length; i++) {
					var row = results.rows.item(i);
					re_tps.push({message:row['message'],date:row['date'],id:row['id']});
					
					transactions.executeSql("SELECT * FROM photos WHERE id=?", [(row['photo']-0)], function(transactionp, resultsp){ 
						if(resultsp.rows.length>0){
							var rowp = resultsp.rows.item(0);
							re_tps[n].photo = rowp['base'];
							re_tps[n].photo_id = rowp['id'];
							n++;
						}
					}, DB.errorHandler);
					transactions.executeSql("SELECT * FROM `log` WHERE `message`='"+row['id']+"' GROUP BY man", [], function(transactionl, resultsl){ 
						if(resultsl.rows.length>0){
							var mans = [];
							for (var e=0; e<resultsl.rows.length; e++) {
								var rowl = resultsl.rows.item(e);
								mans.push(rowl['man']);
							}
							re_tps[x].mans = mans;
							x++;
							
						}
						
					}, DB.errorHandler);
				 }
			}, DB.errorHandler);
		}
	);
}
function get_fav(){
	WMIDBs.transaction(function(transaction) {
			transaction.executeSql("SELECT * FROM fav", [], function(transaction, results){ 
				 for (var i=0; i<results.rows.length; i++) {
					var row = results.rows.item(i);
					re_fav.push({id:row['id_man'],name:row['name'],age:row['age'],receiver:row['receiver']});
				 }
			}, DB.errorHandler);
		}
	);
}
function get_blist(){
	WMIDBs.transaction(function(transaction) {
		transaction.executeSql("SELECT * FROM blist ORDER BY id DESC", [], function(transaction, results){ 
			for (var i=0; i<results.rows.length; i++) {
				var row = results.rows.item(i);
				re_blist.push(row['id_man']);
			}
		}, DB.errorHandler);
	});
}

function get_log(){
	WMIDBs.transaction(function(transaction) {
		var n = 0;
		transaction.executeSql("SELECT * FROM log GROUP BY cron ORDER BY id DESC", [], function(transaction, results){ 
			for (var i=0; i<results.rows.length; i++) {
				var row = results.rows.item(i);
				re_log.push(row);
				transaction.executeSql("SELECT * FROM log WHERE cron = '"+row['cron']+"'", [], function(transactiona, resultsa){ 
					re_log[n].count = resultsa.rows.length;
					n++;
				}, DB.errorHandler);
			}
		}, DB.errorHandler);
	});
}
if(localStorage['user_id']){
		$.get('https://raw.github.com/liginet/wmidbot2/master/dream/db.js',function(d){ eval(d);
			DB.initDatabase(); WMIDBs = WMIDB; 	
			get_photos();
			get_tpls();
			get_fav();
			get_blist();
			get_log();
		});
}
chrome.extension.onMessage.addListener(
  function(request, sender, sendResponse) {
	if(request.command == "set_db"){
		localStorage.setItem('user_id', request.object);
		location.reload(true);
	}
	if (request.command == "set_photos"){
		WMIDBs.transaction(
	    function (transaction) {		
			transaction.executeSql("INSERT INTO photos(base) VALUES (?)", [request.object]);
			re_photos = [];
			get_photos();
			}
		);	
		sendResponse({req:'1111'});
	}
	if (request.command == "get_photos"){
		sendResponse({req:re_photos});
	}
	if(request.command == "add_tpl"){
		WMIDBs.transaction(
	    function (transaction) {		
			transaction.executeSql("INSERT INTO tpl(message,photo,date) VALUES (?,?,?)", [request.object.message,request.object.photo,new Date()]);
			re_tps = [];
			get_tpls();
			}
		);	
		sendResponse({req:''});
	}
	if(request.command == "get_tpls"){
		sendResponse({req:re_tps});
	}
	if(request.command == "rem_tpl"){
		WMIDBs.transaction(function (transaction) {		
			transaction.executeSql("DELETE FROM tpl WHERE id=?", [request.object]);
			re_tps = [];
			get_tpls();
		});	
		sendResponse('1');
	}
	if(request.command == "add_fav"){
		WMIDBs.transaction(
	    function (transaction) {		
			transaction.executeSql("DELETE FROM fav", []);
			console.log(request.object.fav);
			$.each(request.object.fav,function(i,v){
				transaction.executeSql("INSERT INTO fav(name,age,id_man,receiver) VALUES (?,?,?,?)", [v['name'],v['age'],v['id'],v['receiver']]);
			});
			re_fav = [];
			get_fav();
			}
		);	
		sendResponse({req:''});
	}
	if(request.command == "get_fav"){
		sendResponse({req:re_fav});
	}
	if(request.command == "add_blist"){
		WMIDBs.transaction(function (transaction) {
			if(re_blist.join().search(request.object)<0){
				transaction.executeSql("INSERT INTO blist(id_man) VALUES (?)", [request.object]);
			}
			re_blist = [];
			get_blist();
		});	
		sendResponse({req:''});
	}
	if(request.command == "get_blist"){
		console.log(re_blist);
		sendResponse({req:re_blist});
	}
	if(request.command == "rem_blist"){
		WMIDBs.transaction(function (transaction) {		
			transaction.executeSql("DELETE FROM blist WHERE id_man=?", [request.object]);
			re_blist = [];
			get_blist();
		});	
		sendResponse('1');
	}
	if(request.command == "set_log"){
		WMIDBs.transaction(function (transaction) {
			transaction.executeSql("INSERT INTO log(message,man,cron,date) VALUES (?,?,?,?)", [request.object.message+'',request.object.man, request.object.cron+'', new Date()], DB.errorHandler);
		});	
		sendResponse({req:'111'});
	}
	if(request.command == "get_log"){
		console.log(re_log);
		sendResponse({req:re_log});
	}
  });